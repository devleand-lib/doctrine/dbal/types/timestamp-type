# Doctrine DBAL Timestamp type
Library for cross-platform timestamp support for Doctrine DBAL.

## How it works?
The Timestamp type is inherited from the Doctrine DateTime type. When saving/retrieving a value, it converts the value to a date in UTC format. Thus, for platforms without direct support of the timestamp type, support is achieved by storing the value in the UTC format in the datetime field.

## Installation
```bash
composer require devleand/doctrine-dbal-timestamp-type
```

And add the type to `Doctrine\DBAL`:
```php
\Doctrine\DBAL\Types\Type::addType('timestamp', 'Devleand\Doctrine\DBAL\Types\TimestampType');
```

### Symfony
Add this to `config/packages/doctrine.yaml`:
```yaml
doctrine:
    dbal:
        types:
            timestamp: Devleand\Doctrine\DBAL\Types\TimestampType
        mapping_types:
            timestamp: timestamp
```
Example of complete file content:
```yaml
doctrine:
    dbal:
        url: '%env(resolve:DATABASE_URL)%'
        types:
            timestamp: Devleand\Doctrine\DBAL\Types\TimestampType
        mapping_types:
            timestamp: timestamp

    orm:
        auto_generate_proxy_classes: true
        naming_strategy: doctrine.orm.naming_strategy.underscore_number_aware
        auto_mapping: true
        mappings:
            App:
                is_bundle: false
                type: annotation
                dir: '%kernel.project_dir%/src/Entity'
                prefix: 'App\Entity'
                alias: App

```
