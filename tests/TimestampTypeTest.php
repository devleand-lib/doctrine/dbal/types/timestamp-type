<?php

declare(strict_types=1);

namespace Devleand\Doctrine\DBAL\Tests\Types;

use DateTime;
use DateTimeZone;
use Devleand\Doctrine\DBAL\Types\TimestampType;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\ConversionException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class TimestampTypeTest extends TestCase
{
    /**
     * @var AbstractPlatform|MockObject
     */
    private $platform;

    protected function setUp(): void
    {
        $this->platform = $this->createMock(AbstractPlatform::class);
    }

    public function testGetSQLDeclarationWhenPlatformDoesNotSupport(): void
    {
        $this->platform
            ->expects($this->once())
            ->method('getDateTimeTypeDeclarationSQL')
            ->willThrowException(Exception::notSupported('getDateTimeTypeDeclarationSQL'));
        $type = new TimestampType();

        $this->expectExceptionObject(Exception::notSupported('getDateTimeTypeDeclarationSQL'));

        $type->getSQLDeclaration([], $this->platform);
    }

    public function testGetSQLDeclaration(): void
    {
        $declaration = 'TEST DATETIME';
        $this->platform
            ->expects($this->once())
            ->method('getDateTimeTypeDeclarationSQL')
            ->willReturn($declaration);
        $type = new TimestampType();

        $actualDeclaration = $type->getSQLDeclaration([], $this->platform);

        $this->assertEquals($declaration, $actualDeclaration);
    }

    /**
     * @dataProvider provideConvertToDatabaseValueData
     *
     * @param mixed       $value
     * @param string|null $expected
     * @param string|null $dateTimeFormatString
     *
     * @throws ConversionException
     */
    public function testConvertToDatabaseValue($value, ?string $expected, ?string $dateTimeFormatString): void
    {
        $this->platform
            ->method('getDateTimeFormatString')
            ->willReturn($dateTimeFormatString);
        $type = new TimestampType();

        $actual = $type->convertToDatabaseValue($value, $this->platform);

        $this->assertEquals($actual, $expected);
    }

    /**
     * @dataProvider provideConvertToDatabaseValueWhenInvalidValueTypeData
     *
     * @param mixed $value
     *
     * @throws ConversionException
     */
    public function testConvertToDatabaseValueWhenInvalidValueType($value): void
    {
        $type = new TimestampType();

        $this->expectExceptionObject(ConversionException::conversionFailedInvalidType(
            $value,
            $type->getName(),
            ['null', 'DateTime']
        ));

        $type->convertToDatabaseValue($value, $this->platform);
    }

    /**
     * @dataProvider provideConvertToPHPValueData
     *
     * @param mixed          $value
     * @param \DateTime|null $expected
     * @param string|null    $dateTimeFormatString
     *
     * @throws ConversionException
     */
    public function testConvertToPHPValue($value, ?DateTime $expected, ?string $dateTimeFormatString): void
    {
        $this->platform
            ->method('getDateTimeFormatString')
            ->willReturn($dateTimeFormatString);
        $type = new TimestampType();

        $actual = $type->convertToPHPValue($value, $this->platform);

        $this->assertEquals($actual, $expected);
    }

    /**
     * @dataProvider provideConvertToPHPValueWhenInvalidDateTimeData
     *
     * @param mixed       $value
     * @param string|null $dateTimeFormatString
     *
     * @throws ConversionException
     */
    public function testConvertToPHPValueWhenInvalidDateTime($value, ?string $dateTimeFormatString): void
    {
        $this->platform
            ->method('getDateTimeFormatString')
            ->willReturn($dateTimeFormatString);
        $type = new TimestampType();

        $this->expectExceptionObject(ConversionException::conversionFailedFormat(
            $value,
            $type->getName(),
            $dateTimeFormatString
        ));

        $type->convertToPHPValue($value, $this->platform);
    }

    /**
     * @throws \Exception
     */
    public function provideConvertToDatabaseValueData(): array
    {
        return [
            'Value is null' => [null, null, null],
            'Value is DateTime with America/Toronto tz and Y-m-d H:i:s format' => [
                new DateTime('2021-03-21 12:04:35', new DateTimeZone('America/Toronto')),
                '2021-03-21 16:04:35',
                'Y-m-d H:i:s'
            ],
            'Value is DateTime with America/Toronto tz and Y-m-d H:i format' => [
                new DateTime('2021-03-21 12:05:35', new DateTimeZone('America/Toronto')),
                '2021-03-21 16:05',
                'Y-m-d H:i'
            ],
            'Value is DateTime with UTC tz and Y-m-d H:i:s format' => [
                new DateTime('2021-12-28 18:24:59', new DateTimeZone('UTC')),
                '2021-12-28 18:24:59',
                'Y-m-d H:i:s'
            ]
        ];
    }

    public function provideConvertToDatabaseValueWhenInvalidValueTypeData(): array
    {
        return [
            'Value is string' => ['test string'],
            'Value is boolean' => [true],
            'Value is object' => [new \stdClass()]
        ];
    }

    public function provideConvertToPHPValueData(): array
    {
        return [
            'Value is null' => [null, null, null],
            'Value is DateTime with America/Toronto tz' => [
                new DateTime('2021-03-21 12:04:35', new DateTimeZone('America/Toronto')),
                new DateTime('2021-03-21 16:04:35', new DateTimeZone('UTC')),
                null
            ],
            'Value is DateTime with UTC tz' => [
                new DateTime('2021-03-21 12:04:35', new DateTimeZone('UTC')),
                new DateTime('2021-03-21 12:04:35', new DateTimeZone('UTC')),
                null
            ],
            'Value is datetime string in Y-m-d H:i:s format' => [
                '2021-10-29 23:00:35',
                new DateTime('2021-10-29 23:00:35', new DateTimeZone('UTC')),
                'Y-m-d H:i:s'
            ],
            'Value is datetime string in Y-m-d H:i format' => [
                '2021-10-29 23:43',
                new DateTime('2021-10-29 23:43', new DateTimeZone('UTC')),
                'Y-m-d H:i'
            ]
        ];
    }

    public function provideConvertToPHPValueWhenInvalidDateTimeData(): array
    {
        return [
            'Datetime string has invalid format' => ['2021-10-29 23:00', 'Y-m-d H:i:s'],
            'Datetime string is invalid' => ['dsfskdfklsdkl', 'Y-m-d H:i:s'],
            'Datetime format is invalid' => ['2021-10-29 23:00:23', 'skeiworjwofkdms342']
        ];
    }
}
